@extends('welcome')

@section('inputForm')
<!-- **************************************DESCRIPTION********************************************** 
	just an input form that calls a POST method to url/submit
	************************************************************************************************
-->
<div class="col-md-8 col-md-offset-2">
	<div id="form-contents" class="col-md-12 col-lg-12">
    	<form class="form-horizontal" role="form" method="POST" action="{{ url('/submit') }}">
    		<div class="form-group">
    			<label class="col-md-6 col-lg-4 control-label">Message</label>
    			<div class="col-md-6 col-lg-6">
    				<input type="msg" class="form-control" name="msg" value="{{ old('msg') }}" required>
        		</div>
        	</div>
        	<div class="form-group">
        		<div class="col-md-6 col-md-offset-4">
        			<button type="submit" class="btn btn-primary">Go</button>
        		</div>
        	</div>
        </form>
	</div>
</div>


@endsection
