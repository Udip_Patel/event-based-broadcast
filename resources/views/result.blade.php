

<!-- **************************************DESCRIPTION********************************************** 
	Webpage that continously checks for a 'user-event' on channel 'channel-name' and updates the text in div with id = notificationText
	ALL THE FRONT-END PUSHER CODE NEEDED TO BROADCAST AN EVENT IS DETAILED IN THIS FILE
	************************************************************************************************
-->
<html>
	<head>
		<script src="//js.pusher.com/2.2/pusher.min.js" type="text/javascript"></script>
    	<script src="//code.jquery.com/jquery-2.1.3.min.js" type="text/javascript"></script>
	
	
	
		<title>Result</title>
		<script>
			var pusher = new Pusher('e16d29c2f5c62aa8cb5c');
			var channel = pusher.subscribe('channel-name');
			channel.bind('user-event', function(data){
				$('#notificationText').text(data.user);
				alert(data.user);
				console.log(data.user);
			});
		</script>
	</head>
	<body>
		<div class="notification" id="notificationText">
	
		</div>
	</body>


</html>




