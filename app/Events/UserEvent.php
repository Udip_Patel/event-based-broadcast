<?php

namespace App\Events;

use App\Events\Event;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $usr;//the user property
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        //set the property to be the same as the parameter just passed in
    	$this->usr = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['channel-name'];
    }
    
    //REPLACES the name of the event that is checked for by the front-end javascript code
    //***THIS METHOD ALLOWS YOU TO REFER TO A UserEvent object as 'user-event', making it so that the variable names are consistent between front and back end 
    //BY DEFAULT: this event is referred to as: 'App\\Events\\UserEvent' from a javascript function
    public function broadcastAs(){
    	return 'user-event';
    }
    
    //BY DEFAULT: the public property $usr will be serialized and sent, but this method allows us to override what is sent
    public function broadcastWith(){
    	return ['user' => $this->usr->name];
    }
}
