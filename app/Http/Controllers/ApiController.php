<?php

namespace App\Http\Controllers;

use Request;
use App\User;
use Illuminate\Support\Facades\Event;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Pusher;
use App\Events\UserEvent;


class ApiController extends Controller
{
    //submit function
    public function submitMessage(){
    	$input = Request::all();
    	
    	//initialize pusher -- no need when using EVENTS
    	//$pusher = new Pusher(env('PUSHER_KEY'), env('PUSHER_SECRET'), env('PUSHER_APP_ID'));
 		
    	$data['msg'] = $input['msg'];
 	
    	//TRIGGER THE EVENT -- this method is interchangeable with event(new Event...) or Event::fire(new Event..)
    	//$pusher->trigger('channel-name', 'user-event', $data);
    	
    	//creating and saving a user instance, NECESSARY, will get an error if user is not saved
    	$usr = new User();
    	$usr->name = $this->generateRandomString(5);
    	$usr->email = $this->generateRandomString(6) .'@gmail.com';
    	$usr->save();
    	//FIRE THE EVENT BY PASSING IN THE newly created user
    	Event::fire(new UserEvent($usr));
    	
    	
    	//redirect to the same webpage
    	return redirect('/submit');
    }
    
    
    
       
    //returns the prompt view --> URL/SUBMIT
    public function promptForMessage(){
    	return view('prompt');
    }
    //returns the result view --> URL/DASHBOARD
    public function returnResult(){
    	return view('result');
    }
    
    
    //USED TO GENERATE RANDOM EMAIL ADDRESSES
    function generateRandomString($length = 6) {
    
    	$characters = 'ABCDFGHJKLMNPQRSTVWXYZ0123456789';
    
    	$charactersLength = strlen($characters);
    	$randomString = '';
    	for ($i = 0; $i < $length; $i++) {
    		$randomString .= $characters[rand(0, $charactersLength - 1)];
    	}
    	return $randomString;
    }
}
